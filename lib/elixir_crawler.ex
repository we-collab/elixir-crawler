defmodule ElixirCrawler do
  alias ElixirCrawler.JobOpening

  use HTTPoison.Base

  def create_job(job_vacancy_block) do
    title = find_title(job_vacancy_block)
    company = find_company(job_vacancy_block)
    salary = find_salary(job_vacancy_block)
    location = find_location(job_vacancy_block)

    #"Vaga: #{ title }, Empresa: #{ company }, Salário: #{ salary }, Local: #{ location }"
    "#{ title }; #{ company }; #{ salary }; #{ location }\n"
    #JobOpening.build(title, company, salary, location)
  end

  def crawl(url) do
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        block_job(body)
      {:ok, %HTTPoison.Response{status_code: 301}} ->
        IO.puts "Not found"
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect reason
    end
  end

  def crawlAllPages(url, numOfPages) do
    File.write("vagas.csv", "Vaga; Empresa; Salário; Local\n")

    allUrls = [url]
    #numOfPages = 5
    if numOfPages > 0 do
      appendUrlList(allUrls, url, numOfPages)
      |> Enum.map(fn urls -> crawl(urls) end)
    else 
      appendToFile("Nenhuma pagina de vagas foi selecionada")
    end
  end

  def appendUrlList(allUrls, url, numOfPages) when numOfPages == 1 do
    Enum.concat(allUrls, [])
  end

  def appendUrlList(allUrls, url, numOfPages) when numOfPages > 1 do
    
    urlNextPage = generateNextUrl(url)

    Enum.concat(allUrls, [urlNextPage])
    |> appendUrlList(urlNextPage, numOfPages - 1)
  end

  def generateNextUrl(url) do 
    numCurrentPage = String.slice(url, -2, 2)
    |> String.to_integer()

    numCurrentPageStr = Integer.to_string(numCurrentPage + 10)
    
    urlBeginning = String.slice(url, 0, String.length(url) - 2)

    "#{urlBeginning}#{numCurrentPageStr}"
  end  

  defp block_job(body) do
    vagas = Floki.parse_document!(body)
    |> Floki.find(".jobsearch-SerpJobCard")
    |> Enum.map(fn job_vacancy -> create_job(job_vacancy) end)

    #File.write("vagas.txt", vagas)
    appendToFile(vagas)
  end

  def appendToFile(vaga) do
    {:ok, file} = File.open("vagas.csv", [:append])
    saveResultsToFile(file, vaga)
    File.close(file)
  end
  
  def saveResultsToFile(file, vaga) do
    IO.binwrite(file, vaga)
  end

  defp find_title(block) do
    Floki.find(block, "h2.title a")
    |> Floki.attribute("title")
  end

  defp find_company(block) do
    Floki.find(block, ".sjcl div .company")
    |> Floki.text
    |> String.trim("\n")
  end

  defp find_location(block) do
    Floki.find(block, ".sjcl div")
    |> Floki.attribute("data-rc-loc")
  end

  defp find_salary(block) do
    Floki.find(block, ".salarySnippet .salary .salaryText")
    |> Floki.text
    |> String.trim("\n")
  end

  defp write_to_file(job_map) do
    title = Map.get(job_map, :title)
    company = Map.get(job_map, :company)
    salary = Map.get(job_map, :salary)
    location = Map.get(job_map, :location)
  end
end
