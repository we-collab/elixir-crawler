defmodule ElixirCrawler.JobOpening do
  @required_keys [:title, :company, :salary, :location]
  defstruct @required_keys

  def build(title, company, salary, location) do
    %ElixirCrawler.JobOpening{
      title: title,
      company: company,
      salary: salary,
      location: location
    }
  end
end

