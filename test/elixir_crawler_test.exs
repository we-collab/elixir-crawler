defmodule ElixirCrawlerTest do
  use ExUnit.Case

  test "visits site" do
    url = "https://github.com/"
    assert ElixirCrawler.crawl(url) == "Built for developersMGM Resorts InternationalNationwideSAPSpotifyRussell Keith-MageeKris NovaEvan YouJess Frazelle"
  end

end
